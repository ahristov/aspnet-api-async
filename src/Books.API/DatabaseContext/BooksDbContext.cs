﻿using Books.API.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace Books.API.DatabaseContext
{
    public class BooksDbContext : DbContext
    {
        public BooksDbContext(DbContextOptions options)
            : base(options)
        { }

        public DbSet<Book> Books { get; set; }

        public DbSet<Author> Authors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Author>().HasData(
                new Author { Id = Guid.Parse("1a2b22f5-e79e-492b-874a-4551cacb5237"), FirstName = "Lisa", LastName = "Wingate", },
                new Author { Id = Guid.Parse("2a2c9308-f79a-45ee-857a-f9a955bc7ee7"), FirstName = "Karen", LastName = "Frazier", },
                new Author { Id = Guid.Parse("3ad134ea-3cce-45bf-aaef-94419d48ef21"), FirstName = "Michael", LastName = "C. Grumley", },
                new Author { Id = Guid.Parse("4a0f96e8-2113-4ecd-b6f9-ce6c6cd21b51"), FirstName = "Douglas", LastName = "Adams", }
            );

            modelBuilder.Entity<Book>().HasData(
                new Book
                {
                    Id = Guid.Parse("1b42e157-536a-4796-835a-580722b90e21"),
                    AuthorId = Guid.Parse("1a2b22f5-e79e-492b-874a-4551cacb5237"),
                    Title = "Before We Were Yours",
                    Description = "A [story] of a family lost and found... A poignant, engrossing tale about sibling love and the toll of secrets.",
                },
                new Book
                {
                    Id = Guid.Parse("2b42e157-536a-4796-835a-580722b90e21"),
                    AuthorId = Guid.Parse("1a2b22f5-e79e-492b-874a-4551cacb5237"),
                    Title = "The Book of Lost Friends",
                    Description = "Emphasizing throughout that stories matter and should never go untold, [Lisa] Wingate has written an absorbing historical for many readers... Enthralling and ultimately heartening.",
                },
                new Book
                {
                    Id = Guid.Parse("3b42e157-536a-4796-835a-580722b90e21"),
                    AuthorId = Guid.Parse("2a2c9308-f79a-45ee-857a-f9a955bc7ee7"),
                    Title = "Crystals for Beginners: The Guide to Get Started with the Healing Power of Crystals",
                    Description = "Discover how crystals and healing stones can help you fight stress, cope with anxiety, and more as you explore the basics of crystal energy healing with this beginner’s guide. With simple guidance, you’ll learn to curate your own crystal collection, and detailed crystal profiles will help you choose the crystal that works best for your healing.",
                },
                new Book
                {
                    Id = Guid.Parse("4b42e157-536a-4796-835a-580722b90e21"),
                    AuthorId = Guid.Parse("2a2c9308-f79a-45ee-857a-f9a955bc7ee7"),
                    Title = "Complete Reiki: The All-in-One Reiki Manual for Deep Healing and Spiritual Growth",
                    Description = "Reiki exists in abundance all around us, and everyone can benefit from its warm, loving energy for balance and healing. Learn how to harness this spiritual power with Complete Reiki, the all-in-one Reiki resource for deep healing and spiritual growth.",
                },
                new Book
                {
                    Id = Guid.Parse("5b42e157-536a-4796-835a-580722b90e21"),
                    AuthorId = Guid.Parse("3ad134ea-3cce-45bf-aaef-94419d48ef21"),
                    Title = "The Last Monument",
                    Description = "Outside Denver, Colorado, Joe Rickards stands over a small aircraft wreckage, studying burnt remains still smoldering in a field of freshly fallen snow...an investigator for the NTSB, working to carefully roll back the last several hours and identify the cause of the accident.",
                }
            );
        }
    }
}
