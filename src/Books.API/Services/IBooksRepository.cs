﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Books.API.Services
{
    public interface IBooksRepository
    {
        Task<Entities.Book> GetBookAsync(Guid id);
        Entities.Book GetBook(Guid id);

        Task<IEnumerable<Entities.Book>> GetBooksAsync();
        Task<IEnumerable<Entities.Book>> GetBooksAsync(IEnumerable<Guid> bookIds);
        IEnumerable<Entities.Book> GetBooks();

        Task<ExternalModels.BookCover> GetBookCoverAsync(string coverId);
        Task<IEnumerable<ExternalModels.BookCover>> GetBookCoversAsync(Guid bookId);

        void AddBook(Entities.Book bookToAdd);

        Task<bool> SaveChangesAsync();
    }
}
