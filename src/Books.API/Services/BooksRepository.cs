﻿using Books.API.DatabaseContext;
using Books.API.Entities;
using Books.API.ExternalModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Books.API.Services
{
    public class BooksRepository : IBooksRepository, IDisposable
    {
        private BooksDbContext _context;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<BooksRepository> _logger;
        private CancellationTokenSource _cancellationTokenSource;

        public BooksRepository(
            BooksDbContext context,
            IHttpClientFactory httpClientFactory,
            ILogger<BooksRepository> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger)); ;
        }

        public async Task<Book> GetBookAsync(Guid id)
        {
            return await _context.Books
                .Include(b => b.Author)
                .FirstOrDefaultAsync(x => x.Id.Equals(id));
        }
        public Book GetBook(Guid id)
        {
            return _context.Books
                .Include(b => b.Author)
                .FirstOrDefault(x => x.Id.Equals(id));
        }

        public async Task<IEnumerable<Book>> GetBooksAsync()
        {
            //await _context.Database.ExecuteSqlRawAsync("WAITFOR DELAY '00:00:01';");
            return await _context.Books
                .Include(b => b.Author)
                .ToListAsync();
        }
        public async Task<IEnumerable<Book>> GetBooksAsync(IEnumerable<Guid> bookIds)
        {
            return await _context.Books.Where(b => bookIds.Contains(b.Id))
                .Include(b => b.Author)
                .ToListAsync();
        }

        public IEnumerable<Book> GetBooks()
        {
            //_context.Database.ExecuteSqlRaw("WAITFOR DELAY '00:00:01';");
            return _context.Books
                .Include(b => b.Author)
                .ToList();
        }

        public async Task<BookCover> GetBookCoverAsync(string coverId)
        {
            var httpClient = _httpClientFactory.CreateClient();

            // pass thru a dummy name
            var response = await httpClient.GetAsync($"https://localhost:5011/api/bookcovers/{coverId}");

            if (response.IsSuccessStatusCode)
            {
                var bookCover = JsonSerializer.Deserialize<BookCover>(
                    await response.Content.ReadAsStringAsync(), 
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true,
                    });

                return bookCover;
            }

            _cancellationTokenSource.Cancel();

            return null;

        }

        public async Task<IEnumerable<ExternalModels.BookCover>> GetBookCoversAsync(Guid bookId)
        {
            var httpClient = _httpClientFactory.CreateClient();
            var bookCovers = new List<BookCover>();

            _cancellationTokenSource = new CancellationTokenSource();

            var bookCoverUrls = new[]
            {
                $"https://localhost:5011/api/bookcovers/{bookId}-dummycover1",
                // $"https://localhost:5011/api/bookcovers/{bookId}-dummycover2?returnFault=true",
                $"https://localhost:5011/api/bookcovers/{bookId}-dummycover3",
                $"https://localhost:5011/api/bookcovers/{bookId}-dummycover4",
                $"https://localhost:5011/api/bookcovers/{bookId}-dummycover5",
            };

            // Create the tasks.
            var downloadBookCoverTasksQuery =
                from bookCoverUrl
                in bookCoverUrls
                select DownloadBookCoverAsync(httpClient, bookCoverUrl, _cancellationTokenSource.Token);

            // Start the tasks.
            var downloadBookCoverTasks = downloadBookCoverTasksQuery.ToList();

            // Return a single tasxk when all task completed.
            // Task.WhenAll() ensures the resulting order is the same as in the list,
            // regardless which order of tasks completition.
            try
            {
                return await Task.WhenAll(downloadBookCoverTasks);
            }
            catch (OperationCanceledException operationCancelledException)
            {
                // log and return empty list of book covers
                // ex.CancellationToken - can use to see what task was cancelled.
                _logger.LogInformation($"{operationCancelledException.Message}");
                foreach (var task in downloadBookCoverTasks)
                {
                    _logger.LogInformation($"Task {task.Id} has status {task.Status}");
                }
                return new List<ExternalModels.BookCover>();
            }
            catch (Exception exception)
            {
                // log and rethrow
                _logger.LogError($"{exception.Message}");
                throw;
            }
        }

        private async Task<BookCover> DownloadBookCoverAsync(
            HttpClient httpClient,
            string bookCoverUrl,
            CancellationToken cancellationToken)
        {
            // throw new Exception("Cannot download book cover");

            var response = await httpClient.GetAsync(bookCoverUrl, cancellationToken);

            if (response.IsSuccessStatusCode)
            {
                var bookCover = JsonSerializer.Deserialize<BookCover>(
                    await response.Content.ReadAsStringAsync(),
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true,
                    });

                return bookCover;
            }

            _cancellationTokenSource.Cancel();

            return null;
        }

        public void AddBook(Book bookToAdd)
        {
            if (bookToAdd == null)
            {
                throw new ArgumentNullException(nameof(bookToAdd));
            }

            _context.Add(bookToAdd);
        }

        public async Task<bool> SaveChangesAsync()
        {
            // return true if 1 or more entities were changed
            return (await _context.SaveChangesAsync() > 0);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_context != null)
                {
                    _context.Dispose();
                    _context = null;
                }

                if (_cancellationTokenSource != null)
                {
                    _cancellationTokenSource.Dispose();
                    _cancellationTokenSource = null;
                }
            }
        }


    }
}
