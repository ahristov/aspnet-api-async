﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Books.API.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Authors",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(2500)", maxLength: 2500, nullable: true),
                    AuthorId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Books_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[,]
                {
                    { new Guid("1a2b22f5-e79e-492b-874a-4551cacb5237"), "Lisa", "Wingate" },
                    { new Guid("2a2c9308-f79a-45ee-857a-f9a955bc7ee7"), "Karen", "Frazier" },
                    { new Guid("3ad134ea-3cce-45bf-aaef-94419d48ef21"), "Michael", "C. Grumley" },
                    { new Guid("4a0f96e8-2113-4ecd-b6f9-ce6c6cd21b51"), "Douglas", "Adams" }
                });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "Id", "AuthorId", "Description", "Title" },
                values: new object[,]
                {
                    { new Guid("1b42e157-536a-4796-835a-580722b90e21"), new Guid("1a2b22f5-e79e-492b-874a-4551cacb5237"), "A [story] of a family lost and found... A poignant, engrossing tale about sibling love and the toll of secrets.", "Before We Were Yours" },
                    { new Guid("2b42e157-536a-4796-835a-580722b90e21"), new Guid("1a2b22f5-e79e-492b-874a-4551cacb5237"), "Emphasizing throughout that stories matter and should never go untold, [Lisa] Wingate has written an absorbing historical for many readers... Enthralling and ultimately heartening.", "The Book of Lost Friends" },
                    { new Guid("3b42e157-536a-4796-835a-580722b90e21"), new Guid("2a2c9308-f79a-45ee-857a-f9a955bc7ee7"), "Discover how crystals and healing stones can help you fight stress, cope with anxiety, and more as you explore the basics of crystal energy healing with this beginner’s guide. With simple guidance, you’ll learn to curate your own crystal collection, and detailed crystal profiles will help you choose the crystal that works best for your healing.", "Crystals for Beginners: The Guide to Get Started with the Healing Power of Crystals" },
                    { new Guid("4b42e157-536a-4796-835a-580722b90e21"), new Guid("2a2c9308-f79a-45ee-857a-f9a955bc7ee7"), "Reiki exists in abundance all around us, and everyone can benefit from its warm, loving energy for balance and healing. Learn how to harness this spiritual power with Complete Reiki, the all-in-one Reiki resource for deep healing and spiritual growth.", "Complete Reiki: The All-in-One Reiki Manual for Deep Healing and Spiritual Growth" },
                    { new Guid("5b42e157-536a-4796-835a-580722b90e21"), new Guid("3ad134ea-3cce-45bf-aaef-94419d48ef21"), "Outside Denver, Colorado, Joe Rickards stands over a small aircraft wreckage, studying burnt remains still smoldering in a field of freshly fallen snow...an investigator for the NTSB, working to carefully roll back the last several hours and identify the cause of the accident.", "The Last Monument" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Books_AuthorId",
                table: "Books",
                column: "AuthorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Books");

            migrationBuilder.DropTable(
                name: "Authors");
        }
    }
}
